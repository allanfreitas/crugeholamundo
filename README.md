CREANDO UNA APP CON CRUGE USANDO GIT
====================================

		**IMPORTANTE**

		En este ejemplo veras la carpeta protected/modules no existe (y cruge tampoco)
		debes copiarla tu mismo en tu proyecto.

		Este ejemplo esta basado en el uso de GIT, pero si no usas git simplemente descargas
		el codigo fuente y lo copias en tu maquina.

1. crear aplicacion en cero.

		/e/apps/yii/framework/yiic webapp /e/code/crugeholamundo

	deberia entonces existir ahora una aplicacion yii en blanco

2. una vez creada la aplicacion, en su raiz crearle un archivo .gitignore:

		.*/
		assets/
		protected/modules/cruge/
		protected/runtime/

3. crear la carpeta protected/modules


4. con la herramienta de consola navegar hacia este directorio:

		$ cd /e/code/crugeholamundo/protected/modules

	usa cualquier herramienta, pero asegurate de pararte en este directorio.

5. clona cruge:

		git clone git@bitbucket.org:christiansalazarh/cruge.git

	esto creara un directorio con el contenido de cruge dentro de el.

6. sigue los pasos de la instalacion que cruge te ofrece y la app queda funcional. necesitaras una
base de datos mysql (o postgre) llamada "crugedemo".  De todos modos, en protected/config/main.php
puedes cambiar esto, pero asegurate que las tablas requeridas por cruge existan para este demo.
