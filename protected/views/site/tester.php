<h1>Tester</h1>

<div style='padding: 3px; background-color: rgb(255,255,230); width: 400px;'>
<p class='hint'>este boton ejecutara el action siteController.php::actionAjaxCrearUsuario()
una vez presionado puedes revisar los usuarios creados consultando la <br/>
	<?php echo Yii::app()->user->ui->getUserManagementAdminLink("lista de usuarios")?>.
</p>
<?php 
	echo CHtml::ajaxButton("Crear un usuario usando el API de Cruge", 
		array('/site/ajaxCrearUsuario'), 
		array ( 
			'success'=>"function(data){ $('#result1').html(data); }",
			'error'=>"function(e){ $('#result1').html('error:'+e.responseText); }"
		),
		array()
	);
?>
<div id='result1'></div>
</div>
